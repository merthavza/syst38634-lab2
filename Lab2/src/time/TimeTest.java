/**
 * @author Mert Havza 991538291
 */


package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	
	@Test
	public void testGetTotalMilisecondsRegular() {
		int totalMiliseconds = Time.getTotalMiliseconds("12:05:05:05");
		assertTrue("Invalid Number of Miliseconds", totalMiliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilisecondsException() {
		int totalMiliseconds = Time.getTotalMiliseconds("12:05:05:0A");
		fail("Invalid Number of Miliseconds");
	}
	
	@Test
	public void testGetTotalMilisecondsBoundaryIn() {
		int totalMiliseconds = Time.getTotalMiliseconds("12:05:05:999");
		assertTrue("Invalid Number of Miliseconds", totalMiliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMilisecondsBoundaryOut() {
		int totalMiliseconds = Time.getTotalMiliseconds("12:05:05:1000");
		fail("Invalid Number of Miliseconds");
	}
	
}
